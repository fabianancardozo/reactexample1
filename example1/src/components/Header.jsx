import logo from './logo.png'
export function Header(){
    
  return(
    <header>
      <nav className='container'>
        <img src={logo} alt="react logo" className="App-logo"/>
        <h1>Corso React 2022</h1>
        <ul className="nav-items">
          <li>Pricing</li>
          <li>About</li>
          <li>Contact</li>
        </ul>    
      </nav>
    </header>
  )
}